const fs = require("fs");

// ---- input parsing ----

const lines = fs
  .readFileSync("input.txt", "utf-8")
  .split(/\r?\n/)
  .filter((line) => !!line);

const time = parseInt(
  lines[0]
    .split(":")[1]
    .split(/\s+/)
    .filter((n) => !!n)
    .reduce((acc, x) => acc + x, "")
);

const distance = parseInt(
  lines[1]
    .split(":")[1]
    .split(/\s+/)
    .filter((n) => !!n)
    .reduce((acc, x) => acc + x, "")
);

// -----------------------

console.log(
  Array(time + 1) // [0..time]
    .fill(0)
    .filter((_, holdTime) => (time - holdTime) * holdTime > distance).length
);
