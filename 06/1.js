const fs = require("fs");

// ---- input parsing ----

const lines = fs
  .readFileSync("input.txt", "utf-8")
  .split(/\r?\n/)
  .filter((line) => !!line);

const times = lines[0]
  .split(":")[1]
  .split(/\s+/)
  .filter((n) => !!n)
  .map((n) => parseInt(n));

const distances = lines[1]
  .split(":")[1]
  .split(/\s+/)
  .filter((n) => !!n)
  .map((n) => parseInt(n));

// -----------------------

console.log(
  times
    .map((time, i) => ({ time, distance: distances[i] }))
    .map(
      ({ time, distance }) =>
        Array(time + 1) // [0..time]
          .fill(0)
          .filter((_, holdTime) => (time - holdTime) * holdTime > distance)
          .length
    )
    .reduce((acc, x) => acc * x, 1)
);
