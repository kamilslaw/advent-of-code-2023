const fs = require("fs");

console.log(
  fs
    .readFileSync("input.txt", "utf-8")
    .split(/\r?\n/)
    .filter((line) => !!line)
    .map(calculateCardValue)
    .reduce((acc, x) => acc + x, 0)
);

function calculateCardValue(line) {
  const winners = line
    .split(":")[1]
    .split("|")[0]
    .split(/\s+/)
    .filter((n) => !!n)
    .map((n) => parseInt(n));
  const numbers = line
    .split("|")[1]
    .split(/\s+/)
    .filter((n) => !!n)
    .map((n) => parseInt(n));
  const numberOfWinners = numbers.filter((n) => winners.includes(n)).length;
  return numberOfWinners ? Math.pow(2, numberOfWinners - 1) : 0;
}
