const fs = require("fs");

console.log(
  fs
    .readFileSync("input.txt", "utf-8")
    .split(/\r?\n/)
    .filter((line) => !!line)
    .map((line) => ({ line, copies: 1 }))
    .map(calculateCardValue)
    .reduce((acc, x) => acc + x, 0)
);

function calculateCardValue({ line, copies }, index, lines) {
  const winners = line
    .split(":")[1]
    .split("|")[0]
    .split(/\s+/)
    .filter((n) => !!n)
    .map((n) => parseInt(n));
  const numbers = line
    .split("|")[1]
    .split(/\s+/)
    .filter((n) => !!n)
    .map((n) => parseInt(n));
  const numberOfWinners = numbers.filter((n) => winners.includes(n)).length;
  for (let i = 1; i <= numberOfWinners; i += 1) {
    const offset = i + index;
    if (offset === lines.length) break;
    lines[offset].copies += copies;
  }
  return copies;
}
