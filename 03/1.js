const fs = require("fs");

const ZERO_CHAR_CODE = 48;
const NINE_CHAR_CODE = 57;

function isDigit(char) {
  const code = char.charCodeAt(0);
  return ZERO_CHAR_CODE <= code && code <= NINE_CHAR_CODE;
}

function isSymbol(char) {
  return !isDigit(char) && char !== ".";
}

const lines = fs
  .readFileSync("input.txt", "utf-8")
  .split(/\r?\n/)
  .filter((line) => !!line)
  .map((line) => line + "."); // Add extra column on right, so no edge cases have to be checked

let sum = 0;
const I = lines[0].length - 1;
const J = lines.length - 1;

for (let j = 0; j <= J; j += 1) {
  let numberStr = "";
  for (let i = 0; i <= I; i += 1) {
    const char = lines[j][i];
    if (isDigit(char)) {
      numberStr += char;
    } else if (numberStr.length) {
      let isAdjacentToSymbol = false;

      // after
      if (isSymbol(char)) isAdjacentToSymbol = true;
      // before
      let indexBefore = i - numberStr.length - 1;
      if (indexBefore >= 0 && isSymbol(lines[j][indexBefore]))
        isAdjacentToSymbol = true;

      // above
      if (indexBefore < 0) indexBefore = 0;
      if (j > 0)
        for (let ii = indexBefore; ii <= i; ii += 1)
          if (isSymbol(lines[j - 1][ii])) isAdjacentToSymbol = true;

      // below
      if (j < J)
        for (let ii = indexBefore; ii <= i; ii += 1)
          if (isSymbol(lines[j + 1][ii])) isAdjacentToSymbol = true;

      if (isAdjacentToSymbol) sum += parseInt(numberStr);
      numberStr = "";
    }
  }
}

console.log(sum);
