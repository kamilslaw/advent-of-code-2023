const fs = require("fs");

const ZERO_CHAR_CODE = 48;
const NINE_CHAR_CODE = 57;

function isDigit(char) {
  const code = char.charCodeAt(0);
  return ZERO_CHAR_CODE <= code && code <= NINE_CHAR_CODE;
}

function isSymbol(char) {
  return !isDigit(char) && char !== ".";
}

const lines = fs
  .readFileSync("input.txt", "utf-8")
  .split(/\r?\n/)
  .filter((line) => !!line)
  .map((line) => line + "."); // Add extra column on right, so no edge cases have to be checked

const I = lines[0].length - 1;
const J = lines.length - 1;

const potentialGears = {};
function fill(j, i, numberStr) {
  const arr = potentialGears[`${j}_${i}`] || [];
  arr.push(parseInt(numberStr));
  potentialGears[`${j}_${i}`] = arr;
}

for (let j = 0; j <= J; j += 1) {
  let numberStr = "";
  for (let i = 0; i <= I; i += 1) {
    const char = lines[j][i];
    if (isDigit(char)) {
      numberStr += char;
    } else if (numberStr.length) {
      let isAdjacentToSymbol = false;

      // after
      if (char === "*") fill(j, i, numberStr);
      // before
      let indexBefore = i - numberStr.length - 1;
      if (indexBefore >= 0 && isSymbol(lines[j][indexBefore]))
        fill(j, indexBefore, numberStr);

      // above
      if (indexBefore < 0) indexBefore = 0;
      if (j > 0)
        for (let ii = indexBefore; ii <= i; ii += 1)
          if (isSymbol(lines[j - 1][ii])) fill(j - 1, ii, numberStr);

      // below
      if (j < J)
        for (let ii = indexBefore; ii <= i; ii += 1)
          if (isSymbol(lines[j + 1][ii])) fill(j + 1, ii, numberStr);

      numberStr = "";
    }
  }
}

console.log(
  Object.values(potentialGears)
    .filter((arr) => arr.length === 2)
    .reduce((acc, x) => acc + x[0] * x[1], 0)
);
