const fs = require("fs");

const baseMap = fs
  .readFileSync("input.txt", "utf-8")
  .split(/\r?\n/)
  .filter((line) => !!line)
  .map((line) => line.replace("S", "O").split(""));
let Y = baseMap.length;
let X = baseMap[0].length;
const R = 9;
let map = array2d(Y * R, X * R, () => ".");

for (let y = 0; y < Y; y += 1)
  for (let x = 0; x < X; x += 1)
    if (baseMap[y][x] === "#")
      for (let rY = 0; rY < R; rY += 1)
        for (let rX = 0; rX < R; rX += 1) map[Y * rY + y][X * rX + x] = "#";
map[Math.floor((Y * R) / 2)][Math.floor((X * R) / 2)] = "O";

Y *= R;
X *= R;

for (let s = 0; s < 458; s += 1) {
  for (let y = 0; y < Y; y += 1)
    map[y] = map[y].map((c) => (c === "O" ? "!" : c));
  const newMap = [...map.map((r) => [...r])];
  for (let y = 0; y < Y; y += 1)
    for (let x = 0; x < X; x += 1)
      if (map[y][x] === "!") {
        if (newMap[y][x] === "!") newMap[y][x] = ".";
        if (x > 0 && map[y][x - 1] !== "#") newMap[y][x - 1] = "O";
        if (y > 0 && map[y - 1][x] !== "#") newMap[y - 1][x] = "O";
        if (x < X - 1 && map[y][x + 1] !== "#") newMap[y][x + 1] = "O";
        if (y < Y - 1 && map[y + 1][x] !== "#") newMap[y + 1][x] = "O";
      }
  map = newMap;
}

fs.writeFileSync("output.txt", map.map((r) => r.join("")).join("\r\n"));

/*
The input shape:
### # ###
## ### ##
# ##### #
 #######
# ##### #
## ### ##
### # ###
After some experiments I've noticed that:
 - the inner diamond is filled in 64 iterations
 - the 3 next diamonds are filled in 196 iterations
 - the 5 next diamonds are filled in 327 iterations
 - the 7 next diamonds are filled in 458 iterations

Those values create the quadratic function:
15281 x^2 - 15122 x + 3737
https://www.wolframalpha.com/input?i=interpolating+polynomial+calculator&assumption=%7B%22F%22%2C+%22InterpolatingPolynomialCalculator%22%2C+%22data%22%7D+-%3E%22%7B3896%2C+34617%2C+95900%2C+187745%7D%22

We had to find the number of "O"-s after 26 501 365 iterations.
26 501 365 - 65 = 26501300  (65 is the starting offset)
26 501 300 Mod 131 = 0      (131 is the difference between 196 and 64, 327 and 196, etc.)
26 501 300 ÷ 131 = 202 300

for x equal to 202 301
15281 x^2 - 15122 x + 3737 = 625382480005896
*/

function array2d(y, x, valueFunc) {
  return Array(y)
    .fill(0)
    .map(() =>
      Array(x)
        .fill(0)
        .map(() => valueFunc())
    );
}
