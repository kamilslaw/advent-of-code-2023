const fs = require("fs");

let map = fs
  .readFileSync("input.txt", "utf-8")
  .split(/\r?\n/)
  .filter((line) => !!line)
  .map((line) => line.replace("S", "O").split(""));
const Y = map.length - 1;
const X = map[0].length - 1;

for (let s = 0; s < 64; s += 1) {
  for (let y = 0; y <= Y; y += 1)
    map[y] = map[y].map((c) => (c === "O" ? "!" : c));
  const newMap = [...map.map((r) => [...r])];
  for (let y = 0; y <= Y; y += 1)
    for (let x = 0; x <= X; x += 1)
      if (map[y][x] === "!") {
        if (newMap[y][x] === "!") newMap[y][x] = ".";
        if (x > 0 && map[y][x - 1] !== "#") newMap[y][x - 1] = "O";
        if (y > 0 && map[y - 1][x] !== "#") newMap[y - 1][x] = "O";
        if (x < X && map[y][x + 1] !== "#") newMap[y][x + 1] = "O";
        if (y < Y && map[y + 1][x] !== "#") newMap[y + 1][x] = "O";
      }
  map = newMap;
}

console.log(
  map.reduce(
    (acc, r) => acc + r.reduce((acc, x) => acc + (x === "O" ? 1 : 0), 0),
    0
  )
);
