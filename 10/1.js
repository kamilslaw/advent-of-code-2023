const fs = require("fs");

// "S" shape is based on my custom input, for the other inputs it could be different
const DIRECTIONS = [
  { op: ({ x, y }) => ({ x: x + 1, y }), from: "-LFS", to: "-J7" },
  { op: ({ x, y }) => ({ x: x - 1, y }), from: "-J7", to: "-LFS" },
  { op: ({ x, y }) => ({ x, y: y + 1 }), from: "|7FS", to: "|LJ" },
  { op: ({ x, y }) => ({ x, y: y - 1 }), from: "|LJ", to: "|7FS" },
];

const lines = fs
  .readFileSync("input.txt", "utf-8")
  .split(/\r?\n/)
  .filter((line) => !!line);

const startY = lines.findIndex((line) => line.includes("S"));
const startX = lines[startY].indexOf("S");

console.log(Math.max(travel({ x: startX, y: startY })));

function get({ x, y }) {
  return lines[y] && lines[y][x];
}

function eqauls(pos1, pos2) {
  return pos1.x === pos2.x && pos1.y === pos2.y;
}

function travel(startPos) {
  const visited = [];
  let distance = 0;
  let pos = startPos;
  while (true) {
    visited.push(pos);
    distance += 1;
    const direction = DIRECTIONS.find(
      (d) =>
        d.from.includes(get(pos)) &&
        d.to.includes(get(d.op(pos))) &&
        !visited.some((v) => eqauls(v, d.op(pos)))
    );
    if (!direction) break;
    pos = direction.op(pos);
  }
  const maxDistance = distance / 2;
  return maxDistance;
}
