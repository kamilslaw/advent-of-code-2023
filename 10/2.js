const fs = require("fs");

// "S" shape is based on my custom input, for the other inputs it could be different
const S = "F";

const DIRECTIONS = [
  { op: ({ x, y }) => ({ x: x + 1, y }), from: "-_LF", to: "-_J7" },
  { op: ({ x, y }) => ({ x: x - 1, y }), from: "-_J7", to: "-_LF" },
  { op: ({ x, y }) => ({ x, y: y + 1 }), from: "|/7F", to: "|/LJ" },
  { op: ({ x, y }) => ({ x, y: y - 1 }), from: "|/LJ", to: "|/7F" },
];

const OUTSIDE_DIRECTIONS = [
  ({ x, y }) => ({ x: x + 1, y }),
  ({ x, y }) => ({ x: x - 1, y }),
  ({ x, y }) => ({ x, y: y + 1 }),
  ({ x, y }) => ({ x, y: y - 1 }),
  ({ x, y }) => ({ x: x + 1, y: y + 1 }),
  ({ x, y }) => ({ x: x - 1, y: y - 1 }),
  ({ x, y }) => ({ x: x + 1, y: y - 1 }),
  ({ x, y }) => ({ x: x - 1, y: y + 1 }),
];

let lines = fs
  .readFileSync("input.txt", "utf-8")
  .split(/\r?\n/)
  .filter((line) => !!line);
const startY = lines.findIndex((line) => line.includes("S"));
const startX = lines[startY].indexOf("S");

lines = fs
  .readFileSync("input.txt", "utf-8")
  .split(/\r?\n/)
  .filter((line) => !!line)
  .map(
    (line) =>
      `#${line
        .split("")
        .map((c) => (c === "S" ? S : c))
        .join("")}#`
  );
lines.unshift("#".repeat(lines[0].length));
lines.push("#".repeat(lines[0].length));

// EXPAND TILES, SO THERE ARE REAL GAPS BETWEEN PATHS (use "#" instead of ".", so original empty files can be recognized)
for (let y = 0; y < lines.length; y += 1) {
  let newLine = "";
  for (let x = 0; x < lines[y].length; x += 1) {
    const str = get({ y, x }) + get({ y, x: x + 1 });
    newLine += ["--", "-J", "-7", "L-", "LJ", "L7", "F-", "FJ", "F7"].includes(
      str
    )
      ? `${get({ y, x })}_`
      : `${get({ y, x })}#`;
  }
  lines[y] = newLine;
}
const len = lines.length * 2 - 1;
for (let y = 0; y < len; y += 2) {
  let newLine = "";
  for (let x = 0; x < lines[y].length; x += 1) {
    const str = get({ y, x }) + get({ y: y + 1, x });
    newLine += ["||", "|L", "|J", "7|", "7L", "7J", "F|", "FL", "FJ"].includes(
      str
    )
      ? "/"
      : "#";
  }
  lines.splice(y + 1, 0, newLine);
}

const pathVisited = travel({ x: startX * 2 + 2, y: startY * 2 + 2 });
// we added the extra outer layer with "#" only, so every outer element can be reached from {0, 0}
const outsideVisited = travelOutside(0, 0, pathVisited);

let result = 0;
for (let y = 0; y < lines.length; y += 1)
  for (let x = 0; x < lines[y].length; x += 1)
    if (
      !["#", "_", "/"].includes(get({ x, y })) &&
      !hasInVisited(pathVisited, { x, y }) &&
      !hasInVisited(outsideVisited, { x, y })
    )
      result += 1;

console.log("final result", result);

function get({ x, y }) {
  return lines[y] && lines[y][x];
}

function travel(startPos) {
  const visited = createVisited();
  let pos = startPos;
  while (true) {
    addToVisited(visited, pos);
    const direction = DIRECTIONS.find(
      (d) =>
        d.from.includes(get(pos)) &&
        d.to.includes(get(d.op(pos))) &&
        !hasInVisited(visited, d.op(pos))
    );
    if (!direction) break;
    pos = direction.op(pos);
  }
  return visited;
}

function travelOutside(startX, startY, pathVisited) {
  const visited = createVisited();
  const q = [{ x: startX, y: startY }];
  while (q.length) {
    const pos = q.pop();
    if (hasInVisited(visited, pos)) continue;
    addToVisited(visited, pos);
    OUTSIDE_DIRECTIONS.filter(
      (d) =>
        d(pos).x >= 0 &&
        d(pos).x < lines[0].length &&
        d(pos).y >= 0 &&
        d(pos).y < lines.length &&
        !hasInVisited(pathVisited, d(pos)) &&
        !hasInVisited(visited, d(pos))
    ).forEach((d) => q.push(d(pos)));
  }
  return visited;
}

// -----------------------

function createVisited() {
  const visited = new Array(lines.length);
  for (let i = 0; i < visited.length; i += 1)
    visited[i] = Array(lines[0].length).fill(false);
  return visited;
}

function hasInVisited(visited, { x, y }) {
  return visited[y] && visited[y][x];
}

function addToVisited(visited, { x, y }) {
  visited[y][x] = true;
}

// -----------------------
