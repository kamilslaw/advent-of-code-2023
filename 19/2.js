const fs = require("fs");

const input = fs.readFileSync("input.txt", "utf-8").split(/\r?\n\r?\n/);

const ranges = {
  x: [1, 4001],
  m: [1, 4001],
  a: [1, 4001],
  s: [1, 4001],
};
const workflows = arrayToObj(
  input[0].split(/\r?\n/).map((line) => ({
    name: line.split("{")[0],
    rules: line
      .replace(/[a-z][><][0-9]*:R,R/g, "R")
      .replace(/[a-z][><][0-9]*:R,R/g, "R")
      .replace(/[a-z][><][0-9]*:R,R/g, "R")
      .replace(/[a-z][><][0-9]*:R,R/g, "R") // reduce unnecessary checks
      .split("{")[1]
      .split("}")[0]
      .split(",")
      .map((str) => {
        if (str === "R") return () => false;
        if (str === "A") return () => true;
        if (!str.includes(":")) return () => str;
        const selector = str[0];
        const value = parseInt(str.substring(2));
        ranges[selector].push(value);
        ranges[selector].push(str[1] === "<" ? value - 1 : value + 1);
        let target = str.split(":")[1];
        if (target === "A") target = true;
        if (target === "R") target = false;
        return str[1] === "<"
          ? (process) => (process[selector] < value ? target : undefined)
          : (process) => (process[selector] > value ? target : undefined);
      }),
  })),
  (w) => w.name
);

Object.keys(ranges).forEach(
  (r) => (ranges[r] = [...new Set(ranges[r])].sort((a, b) => a - b))
);
let combinations = 0;
for (let x = 0; x < ranges.x.length - 1; x += 1) {
  console.time(`${x + 1}/${ranges.x.length - 1}`);
  for (let m = 0; m < ranges.m.length - 1; m += 1)
    for (let a = 0; a < ranges.a.length - 1; a += 1)
      for (let s = 0; s < ranges.s.length - 1; s += 1)
        if (
          process({
            x: ranges.x[x],
            m: ranges.m[m],
            a: ranges.a[a],
            s: ranges.s[s],
          })
        )
          combinations +=
            (ranges.x[x + 1] - ranges.x[x]) *
            (ranges.m[m + 1] - ranges.m[m]) *
            (ranges.a[a + 1] - ranges.a[a]) *
            (ranges.s[s + 1] - ranges.s[s]);
  console.timeEnd(`${x + 1}/${ranges.x.length - 1}`);
}
console.log(combinations);

function process(process) {
  let workflow = "in";
  while (true) {
    for (const rule of workflows[workflow].rules) {
      const result = rule(process);
      if (result !== undefined) {
        if (typeof result == "boolean") return result;
        workflow = result;
        break;
      }
    }
  }
}

function arrayToObj(array, selector) {
  return array.reduce((acc, x) => {
    acc[selector(x)] = x;
    return acc;
  }, {});
}
