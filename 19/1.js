const fs = require("fs");

const input = fs.readFileSync("input.txt", "utf-8").split(/\r?\n\r?\n/);

const workflows = arrayToObj(
  input[0].split(/\r?\n/).map((line) => ({
    name: line.split("{")[0],
    rules: line
      .split("{")[1]
      .split("}")[0]
      .split(",")
      .map((str) => {
        if (str === "R") return () => false;
        if (str === "A") return () => true;
        if (!str.includes(":")) return () => str;
        const selector = str[0];
        const value = parseInt(str.substring(2));
        let target = str.split(":")[1];
        if (target === "A") target = true;
        if (target === "R") target = false;
        return str[1] === "<"
          ? (process) => (process[selector] < value ? target : undefined)
          : (process) => (process[selector] > value ? target : undefined);
      }),
  })),
  (w) => w.name
);

const parts = input[1]
  .split(/\r?\n/)
  .map((line) =>
    JSON.parse(line.replace("{", '{"').replace(/,/g, ',"').replace(/=/g, '":'))
  );

console.log(
  parts
    .filter(process)
    .flatMap(Object.values)
    .reduce((acc, x) => acc + x, 0)
);

function process(process) {
  let workflow = "in";
  while (true) {
    for (const rule of workflows[workflow].rules) {
      const result = rule(process);
      if (result !== undefined) {
        if (typeof result == "boolean") return result;
        workflow = result;
        break;
      }
    }
  }
}

function arrayToObj(array, selector) {
  return array.reduce((acc, x) => {
    acc[selector(x)] = x;
    return acc;
  }, {});
}
