const fs = require("fs");

const layout = fs
  .readFileSync("input.txt", "utf-8")
  .split(/\r?\n/)
  .filter((line) => !!line)
  .map((line) => line.split(""));
const Y = layout.length;
const X = layout[0].length;

let results = [];
for (let i = 0; i < X; i += 1) results.push(runSimulation({ x: i, y: 0 }, "d"));
for (let i = 0; i < X; i += 1)
  results.push(runSimulation({ x: i, y: Y - 1 }, "u"));
for (let i = 0; i < Y; i += 1) results.push(runSimulation({ x: 0, y: i }, "r"));
for (let i = 0; i < Y; i += 1)
  results.push(runSimulation({ x: X - 1, y: i }, "l"));

console.log(Math.max(...results));

function runSimulation(startPos, startDir) {
  const energized = Array(Y)
    .fill(0)
    .map(() => Array(X).fill(0));
  const visited = Array(Y)
    .fill(0)
    .map(() => Array(X).fill(""));

  travel(startPos, startDir);

  return energized.reduce(
    (acc, row) => acc + row.reduce((acc, x) => acc + x, 0),
    0
  );

  function move({ y, x }, dir) {
    return dir === "r"
      ? { y, x: x + 1 }
      : dir === "l"
      ? { y, x: x - 1 }
      : dir === "d"
      ? { y: y + 1, x }
      : { y: y - 1, x };
  }

  function rotateRight(dir) {
    return dir === "r" ? "d" : dir === "d" ? "l" : dir === "l" ? "u" : "r";
  }

  function rotateLeft(dir) {
    return dir === "r" ? "u" : dir === "u" ? "l" : dir === "l" ? "d" : "r";
  }

  function travel({ y, x }, dir) {
    if (x < 0 || x === X || y < 0 || y === Y) return;
    if (visited[y][x].includes(dir)) return;

    energized[y][x] = 1;
    visited[y][x] += dir;

    const c = layout[y][x];
    if (c === ".") {
      travel(move({ y, x }, dir), dir);
    } else if (c === "/") {
      const newDir =
        dir === "u" || dir === "d" ? rotateRight(dir) : rotateLeft(dir);
      travel(move({ y, x }, newDir), newDir);
    } else if (c === "\\") {
      const newDir =
        dir === "u" || dir === "d" ? rotateLeft(dir) : rotateRight(dir);
      travel(move({ y, x }, newDir), newDir);
    } else if (c === "|") {
      if (dir === "u" || dir === "d") travel(move({ y, x }, dir), dir);
      else {
        travel({ y: y - 1, x }, "u");
        travel({ y: y + 1, x }, "d");
      }
    } else if (c === "-") {
      if (dir === "l" || dir === "r") travel(move({ y, x }, dir), dir);
      else {
        travel({ y, x: x - 1 }, "l");
        travel({ y, x: x + 1 }, "r");
      }
    }
  }
}
