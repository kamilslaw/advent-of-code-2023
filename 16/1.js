const fs = require("fs");

const layout = fs
  .readFileSync("input.txt", "utf-8")
  .split(/\r?\n/)
  .filter((line) => !!line)
  .map((line) => line.split(""));

const energized = Array(layout.length)
  .fill(0)
  .map(() => Array(layout[0].length).fill(0));
const visited = Array(layout.length)
  .fill(0)
  .map(() => Array(layout[0].length).fill(""));

travel({ y: 0, x: 0 }, "r");

console.log(
  energized.reduce((acc, row) => acc + row.reduce((acc, x) => acc + x, 0), 0)
);

function move({ y, x }, dir) {
  return dir === "r"
    ? { y, x: x + 1 }
    : dir === "l"
    ? { y, x: x - 1 }
    : dir === "d"
    ? { y: y + 1, x }
    : { y: y - 1, x };
}

function rotateRight(dir) {
  return dir === "r" ? "d" : dir === "d" ? "l" : dir === "l" ? "u" : "r";
}

function rotateLeft(dir) {
  return dir === "r" ? "u" : dir === "u" ? "l" : dir === "l" ? "d" : "r";
}

function travel({ y, x }, dir) {
  if (x < 0 || x === layout[0].length || y < 0 || y === layout.length) return;
  if (visited[y][x].includes(dir)) return;

  energized[y][x] = 1;
  visited[y][x] += dir;

  const c = layout[y][x];
  if (c === ".") {
    travel(move({ y, x }, dir), dir);
  } else if (c === "/") {
    const newDir =
      dir === "u" || dir === "d" ? rotateRight(dir) : rotateLeft(dir);
    travel(move({ y, x }, newDir), newDir);
  } else if (c === "\\") {
    const newDir =
      dir === "u" || dir === "d" ? rotateLeft(dir) : rotateRight(dir);
    travel(move({ y, x }, newDir), newDir);
  } else if (c === "|") {
    if (dir === "u" || dir === "d") travel(move({ y, x }, dir), dir);
    else {
      travel({ y: y - 1, x }, "u");
      travel({ y: y + 1, x }, "d");
    }
  } else if (c === "-") {
    if (dir === "l" || dir === "r") travel(move({ y, x }, dir), dir);
    else {
      travel({ y, x: x - 1 }, "l");
      travel({ y, x: x + 1 }, "r");
    }
  }
}
