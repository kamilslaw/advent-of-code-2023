const fs = require("fs");

const NUMBER_OF_ITERATIONS = 10_000;

const paths = fs
  .readFileSync("input.txt", "utf-8")
  .split(/\r?\n/)
  .filter((line) => !!line)
  .map((line) => {
    const [px, py, pz] = line
      .split(" @ ")[0]
      .split(", ")
      .map((x) => parseInt(x));
    const [vx, vy, vz] = line
      .split(" @ ")[1]
      .split(", ")
      .map((x) => parseInt(x));
    return { px, py, pz, vx, vy, vz };
  });

const positions = paths.map(({ px, py, pz, vx, vy, vz }) =>
  Array(NUMBER_OF_ITERATIONS)
    .fill(0)
    .map((_, i) => ({ i, x: px + vx * i, y: py + vy * i, z: pz + vz * i }))
);

let solutions = [];

for (let i = 0; i < NUMBER_OF_ITERATIONS; i += 1)
  for (let j = 0; j < NUMBER_OF_ITERATIONS; j += 1)
    solutions.push([positions[0][i], positions[1][j]]);

for (let i = 2; i < positions.length; i += 1) {
  solutions = solutions.flatMap((s) =>
    positions[i].filter((p) => inOneLine(s, p)).map((p) => s.concat([p]))
  );
  if (!solutions.length)
    throw Error(
      `No solution found in the first ${NUMBER_OF_ITERATIONS} iterations`
    );
}

console.log(JSON.stringify(solutions));
solutions
  .map(findColisionStartingPoint)
  .filter((result) => !!result)
  .forEach(({ x, y, z }) => console.log(x, y, z, x + y + z));

function inOneLine(points, testedPoint) {
  const a = points[0];
  const b = points[1];
  const c = testedPoint;
  // Vectors
  const ab = { x: a.x - b.x, y: a.y - b.y, z: a.z - b.z };
  const bc = { x: b.x - c.x, y: b.y - c.y, z: b.z - c.z };
  // Rule of Sarrus
  return (
    ab.x * bc.y +
      ab.y * bc.z +
      ab.z * bc.x -
      ab.y * bc.x -
      ab.z * bc.y -
      ab.x * bc.z ===
    0
  );
}

function findColisionStartingPoint(solution) {
  solution.sort((a, b) => a.x - b.x || a.y - b.y || a.z - b.z);

  const dir = solution[0].i < solution[1].i ? "asc" : "desc";
  for (let i = 0; i < solution.length - 1; i += 1)
    if (dir === "asc" && solution[i].i > solution[i + 1].i) return false;
    else if (dir === "desc" && solution[i].i < solution[i + 1].i) return false;

  const getVector = (a, b) => ({
    x: (b.x - a.x) / (b.i - a.i),
    y: (b.y - a.y) / (b.i - a.i),
    z: (b.z - a.z) / (b.i - a.i),
  });
  const vector = getVector(
    dir === "asc" ? solution[0] : solution[1],
    dir === "asc" ? solution[1] : solution[0]
  );
  for (let i = 0; i < solution.length - 1; i += 1) {
    const tmpVector = getVector(
      dir === "asc" ? solution[i] : solution[i + 1],
      dir === "asc" ? solution[i + 1] : solution[i]
    );
    if (
      tmpVector.x !== vector.x ||
      tmpVector.y !== vector.y ||
      tmpVector.z !== vector.z
    )
      return false;
  }
  return dir === "asc"
    ? {
        x: solution[0].x + vector.x * solution[0].i,
        y: solution[0].y + vector.y * solution[0].i,
        z: solution[0].z + vector.z * solution[0].i,
      }
    : {
        x: solution[0].x - vector.x * solution[0].i,
        y: solution[0].y - vector.y * solution[0].i,
        z: solution[0].z - vector.z * solution[0].i,
      };
}
