const fs = require("fs");

const paths = fs
  .readFileSync("input.txt", "utf-8")
  .split(/\r?\n/)
  .filter((line) => !!line)
  .map((line) => {
    const [px, py, pz] = line
      .split(" @ ")[0]
      .split(", ")
      .map((x) => parseInt(x));
    const [vx, vy, vz] = line
      .split(" @ ")[1]
      .split(", ")
      .map((x) => parseInt(x));
    return { px, py, pz, vx, vy, vz };
  });

const min = 200000000000000;
const max = 400000000000000;

let result = 0;
paths.forEach((l, i) =>
  paths.forEach((r, j) => {
    if (i >= j) return;
    const point = getIntersectionPoint(l, r);
    if (point === undefined) return;
    const { x, y } = point;
    if (
      x >= min &&
      x <= max &&
      y >= min &&
      y <= max &&
      isInFuture(l, { x, y }) &&
      isInFuture(r, { x, y })
    )
      result += 1;
  })
);

console.log(result);

function getIntersectionPoint(l, r) {
  const l2 = { x: l.px + l.vx, y: l.py + l.vy };
  const la = (l2.y - l.py) / (l2.x - l.px);
  const lb = l.py - la * l.px;

  const r2 = { x: r.px + r.vx, y: r.py + r.vy };
  const ra = (r2.y - r.py) / (r2.x - r.px);
  const rb = r.py - ra * r.px;

  if (la === ra) return undefined; // parralel

  const x = (rb - lb) / (la - ra);
  const y = la * x + lb;
  return { x, y };
}

function isInFuture(path, { x, y }) {
  if (path.vx > 0 && x < path.px) return false;
  if (path.vx < 0 && x > path.px) return false;
  if (path.vy > 0 && y < path.py) return false;
  if (path.vy < 0 && y > path.py) return false;
  return true;
}
