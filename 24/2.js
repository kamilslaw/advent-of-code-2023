const fs = require("fs");

const paths = fs
  .readFileSync("input.txt", "utf-8")
  .split(/\r?\n/)
  .filter((line) => !!line)
  .map((line) => {
    const [px, py, pz] = line
      .split(" @ ")[0]
      .split(", ")
      .map((x) => parseInt(x));
    const [vx, vy, vz] = line
      .split(" @ ")[1]
      .split(", ")
      .map((x) => parseInt(x));
    return { px, py, pz, vx, vy, vz };
  });

// 3 parralel vectors
const parralel = paths.filter(
  (l) => paths.filter((r) => areParralel(l, r)).length === 3
);
console.log(parralel);

// TODO: usun, nie działa to, nie ma tak niskiego wspólnego

// w pętli:
//  wyznacz kolejne iteracje, który tworzą linię - możemy je wyznaczyć z nww() wektorów, nie mogą być przed ani za początkiem zadnej z półprostych
//  wlasciwie to najpierw policz ile ich jest, bo wektory są dodatnie i ujemne, więc jest to skonczona liczba
// sprawdz po prostu każdy z każdy, od 0 do np. 10_000, nawet nie musi być Z
// potem pomyślisz co zrobic z wynikiem, może da się zmniejszyć dziedzinę
// można nawet podziałać w 2 wymiarach na start

// poszukaj jednak wspólnego prostopadłego no... jak bedzie to nam rozwiązuje
// żeby sobie ułatwić, dla każdego ujemnego wektora odwrócimy to, znajdziemy punkt po drugiej stronie planszy, na końcu odwrócimy nr iteracji
// https://math.stackexchange.com/questions/2218763/how-to-find-lcm-of-two-numbers-when-one-starts-with-an-offset

function areParralel(l, r) {
  return (
    l.vx * r.vy +
      l.vy * r.vz +
      l.vz * r.vx -
      l.vy * r.vx -
      l.vz * r.vy -
      l.vx * r.vz ===
    0
  );
}
