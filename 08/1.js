const fs = require("fs");

// ---- input parsing ----

const lines = fs
  .readFileSync("input.txt", "utf-8")
  .split(/\r?\n/)
  .filter((line) => !!line);

const ins = lines[0]; // instructions
const nodes = {};

for (const line of lines.slice(1)) {
  const node = line.split(" =")[0];
  const left = line.split("(")[1].split(",")[0];
  const right = line.split(", ")[1].replace(")", "");
  nodes[node] = { name: node, left, right };
}

// -----------------------

let steps = 0;
let node = nodes["AAA"];

while (node.name !== "ZZZ")
  node = nodes[ins[steps++ % ins.length] === "L" ? node.left : node.right];

console.log(steps);
