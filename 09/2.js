const fs = require("fs");

console.log(
  fs
    .readFileSync("input.txt", "utf-8")
    .split(/\r?\n/)
    .filter((line) => !!line)
    .map((line) => line.split(" ").map((x) => parseInt(x)))
    .map(getNextValue)
    .reduce((acc, v) => acc + v, 0)
);

function getNextValue(history) {
  const sequences = [history];
  while (!sequences.at(-1).every((v) => v === 0)) {
    const prev = sequences.at(-1);
    sequences.push(
      prev.map((_, i) => prev[i + 1] - prev[i]).slice(0, prev.length - 1)
    );
  }
  sequences.reverse();
  sequences[0].unshift(0);
  for (let i = 1; i < sequences.length; i += 1)
    sequences[i].unshift(sequences[i][0] - sequences[i - 1][0]);
  return sequences.at(-1)[0];
}
