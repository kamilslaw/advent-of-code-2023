const fs = require("fs");

console.log(
  fs
    .readFileSync("input.txt", "utf-8")
    .split(/\r?\n/)
    .filter((line) => !!line)
    .map((line) => {
      const sets = line.split(": ")[1].split("; ");
      const minimums = { red: 0, green: 0, blue: 0 };
      for (const set of sets)
        for (const [number, color] of set.split(", ").map((c) => c.split(" ")))
          if (minimums[color] < parseInt(number))
            minimums[color] = parseInt(number);
      return minimums.red * minimums.green * minimums.blue;
    })
    .reduce((acc, x) => acc + x, 0)
);
