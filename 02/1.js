const fs = require("fs");

const LIMITS = { red: 12, green: 13, blue: 14 };

console.log(
  fs
    .readFileSync("input.txt", "utf-8")
    .split(/\r?\n/)
    .filter((line) => !!line)
    .map((line) => {
      const gameNumber = parseInt(line.split(": ")[0].replace("Game ", ""));
      const sets = line.split(": ")[1].split("; ");
      for (const set of sets)
        for (const [number, color] of set.split(", ").map((c) => c.split(" ")))
          if (LIMITS[color] < parseInt(number)) return 0;
      return gameNumber;
    })
    .reduce((acc, x) => acc + x, 0)
);
