const fs = require("fs");

const MAP = fs
  .readFileSync("input.txt", "utf-8")
  .split(/\r?\n/)
  .filter((line) => !!line)
  .map((line) => line.split(""));
const Y = MAP.length;
const X = MAP[0].length;
const START = { y: 0, x: 1 };
const END = { y: Y - 1, x: X - 2 };
MAP[START.y][START.x] = "#";

let max = 0;
let paths = [{ pos: START, visited: array2d(Y, X, () => false), length: 0 }];
while (paths.length)
  paths = paths.flatMap(({ pos, visited, length }) => {
    if (equals(pos, END)) {
      if (max < length) max = length;
      return [];
    }
    const reachablePositions = getReachablePositions(pos, visited);
    if (reachablePositions.length === 0) return [];
    if (reachablePositions.length === 1) {
      visited.set(pos, true);
      return [{ pos: reachablePositions[0], visited, length: length + 1 }];
    }
    return getReachablePositions(pos, visited).map((newPos) => {
      const v = visited.clone();
      v.set(pos, true);
      return { pos: newPos, visited: v, length: length + 1 };
    });
  });

console.log("Max path length:", max);

function getReachablePositions({ y, x }, visited) {
  const positions = [];
  if (x > 0 && [".", "<"].includes(MAP[y][x - 1]))
    positions.push({ y, x: x - 1 });
  if (x < X - 1 && [".", ">"].includes(MAP[y][x + 1]))
    positions.push({ y, x: x + 1 });
  if (y > 0 && [".", "^"].includes(MAP[y - 1][x]))
    positions.push({ y: y - 1, x });
  if (y < Y - 1 && [".", "v"].includes(MAP[y + 1][x]))
    positions.push({ y: y + 1, x });
  return positions.filter((pos) => !visited.get(pos));
}

function array2d(Y, X, valueFunc) {
  return {
    array: Array(Y * X)
      .fill(0)
      .map(() => valueFunc()),
    get: function ({ y, x }) {
      return this.array[Y * y + x];
    },
    set: function ({ y, x }, value) {
      this.array[Y * y + x] = value;
    },
    clone: function () {
      return { ...this, array: [...this.array] };
    },
  };
}

function equals(pos1, pos2) {
  return pos1.x === pos2.x && pos1.y === pos2.y;
}
