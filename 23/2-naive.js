const fs = require("fs");
const { Map } = require("immutable");

const MAP = fs
  .readFileSync("input.txt", "utf-8")
  .split(/\r?\n/)
  .filter((line) => !!line)
  .map((line) =>
    line.split("").map((c) => (["<", ">", "v", "^"].includes(c) ? "." : c))
  );
const Y = MAP.length;
const X = MAP[0].length;
const START = { y: 0, x: 1 };
const END = { y: Y - 1, x: X - 2 };
MAP[START.y][START.x] = "#";

let i = 0;
let max = 0;
let paths = [{ pos: START, visited: Map(), length: 0 }];
while (paths.length) {
  i += 1;
  if (i % 100 === 0) console.log(new Date(), "i:", i, "paths:", paths.length);
  paths = paths.flatMap(({ pos, visited, length }) => {
    if (equals(pos, END)) {
      if (max < length) max = length;
      return [];
    }
    const reachablePositions = getReachablePositions(pos, visited);
    if (reachablePositions.length === 0) return [];
    if (reachablePositions.length === 1) {
      return [
        {
          pos: reachablePositions[0],
          visited: visited.set(toKey(pos), true),
          length: length + 1,
        },
      ];
    }
    return reachablePositions.map((newPos) => ({
      pos: newPos,
      visited: visited.set(toKey(pos), true),
      length: length + 1,
    }));
  });
}

console.log("Max path length:", max);

function getReachablePositions({ y, x }, visited) {
  const positions = [];
  if (x > 0 && MAP[y][x - 1] === ".") positions.push({ y, x: x - 1 });
  if (x < X - 1 && MAP[y][x + 1] === ".") positions.push({ y, x: x + 1 });
  if (y > 0 && MAP[y - 1][x] === ".") positions.push({ y: y - 1, x });
  if (y < Y - 1 && MAP[y + 1][x] === ".") positions.push({ y: y + 1, x });
  return positions.filter((pos) => !visited.get(toKey(pos)));
}

function toKey({ y, x }) {
  return (y << 20) + x;
}

function equals(pos1, pos2) {
  return pos1.x === pos2.x && pos1.y === pos2.y;
}
