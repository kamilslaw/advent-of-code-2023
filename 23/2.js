const fs = require("fs");
const { Map } = require("immutable");

const MAP = fs
  .readFileSync("input.txt", "utf-8")
  .split(/\r?\n/)
  .filter((line) => !!line)
  .map((line) =>
    line.split("").map((c) => (["<", ">", "v", "^"].includes(c) ? "." : c))
  );
const Y = MAP.length;
const X = MAP[0].length;
const START = { y: 0, x: 1 };
const END = { y: Y - 1, x: X - 2 };
MAP[START.y][START.x] = "#";

// 1) GENERATE THE GRAPH FROM BRANCHES

const graph = [];

let paths = [{ origin: START, pos: START, visited: Map(), length: 0 }];
while (paths.length) {
  paths = paths.flatMap(({ origin, pos, visited, length }) => {
    if (equals(pos, END)) {
      const from = toKey(origin);
      const to = toKey(pos);
      const l = Math.min(from, to);
      const r = Math.max(from, to);
      if (!graph.some(({ from, to }) => from === l && to === r))
        graph.push({ from, to, length }); //, pos1: origin, pos2: pos });
      return [];
    }

    const reachablePositions = getReachablePositions(pos, visited);
    if (reachablePositions.length === 0) return [];
    if (reachablePositions.length === 1) {
      return [
        {
          origin,
          pos: reachablePositions[0],
          visited: visited.set(toKey(pos), true),
          length: length + 1,
        },
      ];
    }

    const from = toKey(origin);
    const to = toKey(pos);
    const l = Math.min(from, to);
    const r = Math.max(from, to);
    if (!graph.some(({ from, to }) => from === l && to === r)) {
      graph.push({ from, to, length }); //, pos1: origin, pos2: pos });
      return reachablePositions.map((newPos) => ({
        origin: pos,
        pos: newPos,
        visited: visited.set(toKey(pos), true),
        length: 1,
      }));
    }
    return [];
  });
}

function getReachablePositions({ y, x }, visited) {
  const positions = [];
  if (x > 0 && MAP[y][x - 1] === ".") positions.push({ y, x: x - 1 });
  if (x < X - 1 && MAP[y][x + 1] === ".") positions.push({ y, x: x + 1 });
  if (y > 0 && MAP[y - 1][x] === ".") positions.push({ y: y - 1, x });
  if (y < Y - 1 && MAP[y + 1][x] === ".") positions.push({ y: y + 1, x });
  return positions.filter((pos) => !visited.get(toKey(pos)));
}

console.log(new Date(), "Graph size:", Object.keys(graph).length);
console.log("Graph:", JSON.stringify(graph));

// 2) FIND THE LONGEST PATH
const max = findLongestPath(toKey(START), Map());
console.log(new Date(), "result:", max);

function findLongestPath(pos, visited) {
  if (pos === toKey(END)) return 0;
  const neighbours = graph.filter(
    ({ from, to }) =>
      (pos === from && !visited.get(from)) || (pos === to && !visited.get(to))
  );
  return Math.max(
    ...neighbours.map(
      ({ from, to, length }) =>
        length +
        findLongestPath(from === pos ? to : from, visited.set(pos, true))
    )
  );
}

function toKey({ y, x }) {
  return (y << 20) + x;
}

function equals(pos1, pos2) {
  return pos1.x === pos2.x && pos1.y === pos2.y;
}
