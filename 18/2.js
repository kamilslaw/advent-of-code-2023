const fs = require("fs");

const lines = fs
  .readFileSync("input.txt", "utf-8")
  .split(/\r?\n/)
  .filter((line) => !!line);

const operations = lines.map((line) => {
  const hex = line.split("#")[1].split(")")[0];
  const dir = hex[5];
  const len = parseInt(hex.slice(0, 5), 16);
  return dir === "0"
    ? ({ x, y }) => ({ x: x + len, y })
    : dir === "2"
    ? ({ x, y }) => ({ x: x - len, y })
    : dir === "3"
    ? ({ x, y }) => ({ x, y: y - len })
    : ({ x, y }) => ({ x, y: y + len });
});

let points = operations.reduce(
  (acc, op) => acc.concat([op(acc.at(-1))]),
  [{ x: 0, y: 0 }]
);
const minX = Math.min(...points.map((p) => p.x));
const minY = Math.min(...points.map((p) => p.y));
points = points.map(({ x, y }) => ({
  x: x + Math.abs(minX),
  y: y + Math.abs(minY),
}));

// https://en.wikipedia.org/wiki/Shoelace_formula
let meters = 0;
for (let i = 0; i < points.length - 1; i += 1) {
  meters +=
    Math.abs(points[i].x - points[i + 1].x) +
    Math.abs(points[i].y - points[i + 1].y);
  meters += points[i].x * points[i + 1].y - points[i].y * points[i + 1].x;
}
meters /= 2;
meters += 1;

console.log(meters);
