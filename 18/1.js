const fs = require("fs");

const lines = fs
  .readFileSync("input.txt", "utf-8")
  .split(/\r?\n/)
  .filter((line) => !!line);

const operations = lines.map((line) => {
  const dir = line.split(" ")[0];
  const len = parseInt(line.split(" ")[1]);
  return dir === "R"
    ? ({ x, y }) => ({ x: x + len, y })
    : dir === "L"
    ? ({ x, y }) => ({ x: x - len, y })
    : dir === "U"
    ? ({ x, y }) => ({ x, y: y - len })
    : ({ x, y }) => ({ x, y: y + len });
});

// FIND AREA SIZE

let minX = 0;
let maxX = 0;
let minY = 0;
let maxY = 0;
let p = { x: 0, y: 0 };
for (const operation of operations) {
  p = operation(p);
  if (p.x < minX) minX = p.x;
  if (p.x > maxX) maxX = p.x;
  if (p.y < minY) minY = p.y;
  if (p.y > maxY) maxY = p.y;
}

const startP = { x: Math.abs(minX), y: Math.abs(minY) };
maxX += Math.abs(minX);
maxY += Math.abs(minY);
minX = 0;
minY = 0;

// CREATE AREA BORDERS

const area = array2d(maxY + 1, maxX + 1, () => 0);
p = { ...startP };
for (const operation of operations) {
  const newP = operation(p);
  for (let y = Math.min(p.y, newP.y); y <= Math.max(p.y, newP.y); y += 1)
    for (let x = Math.min(p.x, newP.x); x <= Math.max(p.x, newP.x); x += 1)
      area[y][x] = 1;
  p = newP;
}

// FILL AREA

const q = [{ x: startP.x + 1, y: startP.y + 1 }];
while (q.length) {
  const { x, y } = q.pop();
  area[y][x] = 1;
  if (!area[y][x + 1]) q.push({ y, x: x + 1 });
  if (!area[y][x - 1]) q.push({ y, x: x - 1 });
  if (!area[y + 1][x]) q.push({ y: y + 1, x });
  if (!area[y - 1][x]) q.push({ y: y - 1, x });
}

console.log(area.reduce((acc, r) => acc + r.reduce((acc, x) => acc + x, 0), 0));

// --------------------------------

function array2d(y, x, valueFunc) {
  return Array(y)
    .fill(0)
    .map(() =>
      Array(x)
        .fill(0)
        .map(() => valueFunc())
    );
}
