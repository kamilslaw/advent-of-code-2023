const fs = require("fs");

const heatMap = fs
  .readFileSync("input.txt", "utf-8")
  .split(/\r?\n/)
  .filter((line) => !!line)
  .map((line) => line.split("").map((x) => parseInt(x)));
const Y = heatMap.length;
const X = heatMap[0].length;

const start = { y: 0, x: 0 };
const end = { y: Y - 1, x: X - 1 };

const { path, distance } = travel();

console.log(heatMap.map((r) => r.join(" ")).join("\r\n"));
console.log();
console.log(
  heatMap
    .map((r, y) =>
      r
        .map((v, x) => (path.some((p) => p.y === y && p.x === x) ? "X" : v))
        .join(" ")
    )
    .join("\r\n")
);
console.log("Distance:", distance);

// dijkstra, but we can move at most three blocks in a single direction
// starting from (0, 0)
// if vertex is visited again, it means that all the previous path are not optimal, we clear its successors
function travel() {
  const prev = array2d(Y, X, () => []);
  const dist = array2d(Y, X, () => Number.MAX_SAFE_INTEGER);
  dist[start.y][start.y] = 0;
  const q = [];
  for (let y = 0; y < Y; y += 1)
    for (let x = 0; x < X; x += 1) q.push({ x, y });
  while (q.length) {
    q.sort((a, b) => dist[b.y][b.x] - dist[a.y][a.x]);
    const u = q.pop();
    const straightSteps = getPossibleSteps(u, prev[u.y][u.x].at(-1));
    const stepsWithAlternatives = straightSteps.flatMap(({ x, y }) =>
      findLocalAlternatives({ x, y }, u, prev[u.y][u.x])
    );
    // TODO: po dodaniu alternatyw ścieżki są znacznie gorsze xD
    //       ale cały czas poprawne; być może coś robię źle
    //       albo nie rozumiem całego flow i eksploracja różnych opcji ukrywa mi te poprawne
    for (const { x, y, p } of stepsWithAlternatives) {
      let d = dist[u.y][u.x];

      if (isTooLong({ x, y }, u, p)) {
        let r = p.at(-4); // root

        let t = p.at(-3); // to replace
        if (!r) continue;
        let s = getNeighbours(r).find(
          (n) =>
            areDiagNeighbors(t, n) &&
            distanceBetween(n, { x, y }) === distanceBetween(t, { x, y }) &&
            !p.some((pp) => equals(pp, n))
        ); // new one
        if (!s) continue;
        d -= heatMap[t.y][t.x];
        d += heatMap[s.y][s.x];
        p[p.length - 3] = s;

        let isFailure = false;
        let offset = 4;
        while (true) {
          t = p.at(-3 - offset);
          if (!t) break;
          if (!isTooLong(s, r, p.slice(0, p.length - offset))) break;
          r = p.at(-4 - offset);
          if (!r) {
            isFailure = true;
            break;
          }
          s = getNeighbours(r).find(
            (n) =>
              areDiagNeighbors(t, n) &&
              distanceBetween(n, s) === distanceBetween(t, s) &&
              !p.some((pp) => equals(pp, n))
          );
          if (!s) {
            isFailure = true;
            break;
          }
          d -= heatMap[t.y][t.x];
          d += heatMap[s.y][s.x];
          p[p.length - 3 - offset] = s;
          offset += 4;
        }
        if (isFailure) continue;
      }

      const alt = d + heatMap[y][x];
      if (alt < dist[y][x]) {
        prev[y][x] = p.concat([u]);
        dist[y][x] = alt;
      }
    }
  }
  const path = prev[end.y][end.x].concat([end]);
  return {
    path,
    distance: path.slice(1).reduce((acc, { y, x }) => acc + heatMap[y][x], 0),
  };
}

// -------------------------------

function findLocalAlternatives({ x, y }, u, p) {
  const paths = [{ x, y, p: [...p] }];
  const pp = p.concat([u]);
  for (let i = 0; i < 5; i += 1) {
    const pos1 = pp[pp.length - i - 2];
    const pos2 = pp[pp.length - i - 1];
    if (!pos1 || !pos2) return paths;
    const toCheck =
      pos1.y === pos2.y
        ? [
            {
              a: { x: pos1.x, y: pos1.y + 1 },
              b: { x: pos2.x, y: pos2.y + 1 },
            },
            {
              a: { x: pos1.x, y: pos1.y - 1 },
              b: { x: pos2.x, y: pos2.y - 1 },
            },
          ]
        : [
            {
              a: { x: pos1.x + 1, y: pos1.y },
              b: { x: pos2.x + 1, y: pos2.y },
            },
            {
              a: { x: pos1.x - 1, y: pos1.y },
              b: { x: pos2.x - 1, y: pos2.y },
            },
          ];
    toCheck
      .filter(
        ({ a, b }) =>
          isInScope(a) &&
          isInScope(b) &&
          !equals(a, { x, y }) &&
          !equals(b, { x, y }) &&
          !pp.some((ppp) => equals(ppp, a)) &&
          !pp.some((ppp) => equals(ppp, b))
      )
      .forEach(({ a, b }) =>
        paths.push({
          x,
          y,
          p: p
            .slice(0, p.length - i)
            .concat([a, b])
            .concat(p.slice(p.length - i, p.length)),
        })
      );
  }
  return paths;
}

// -------------------------------

function getPossibleSteps({ x, y }, prevPos) {
  return getNeighbours({ x, y }).filter(
    (newPos) => prevPos === undefined || !equals(prevPos, newPos)
  );
}

function getNeighbours({ x, y }) {
  return [
    { x: x, y: y - 1 },
    { x: x, y: y + 1 },
    { x: x + 1, y: y },
    { x: x - 1, y: y },
  ].filter(isInScope);
}

function isInScope({ x, y }) {
  return x >= 0 && x < X && y >= 0 && y < Y;
}

function distanceBetween(pos1, pos2) {
  return Math.abs(pos1.x - pos2.x) + Math.abs(pos1.y - pos2.y);
}

function areNeighbors(pos1, pos2) {
  return distanceBetween(pos1, pos2) === 1;
}

function areDiagNeighbors(pos1, pos2) {
  return (
    distanceBetween(pos1, pos2) === 2 && pos1.x !== pos2.x && pos1.y !== pos2.y
  );
}

function equals(pos1, pos2) {
  return pos1.x === pos2.x && pos1.y === pos2.y;
}

function isTooLong(pos, prevPos, prevPosPrev) {
  const dir = getDir(pos, prevPos);
  const pos3 = prevPosPrev.at(-1);
  if (!pos3 || getDir(prevPos, pos3) !== dir) return false;
  const pos4 = prevPosPrev.at(-2);
  if (!pos4 || getDir(pos3, pos4) !== dir) return false;
  const pos5 = prevPosPrev.at(-3);
  if (!pos5 || getDir(pos4, pos5) !== dir) return false;
  return true;
}

// -------------------------------

function getDir(pos, prevPos) {
  if (pos.x < prevPos.x) return "l";
  if (pos.x > prevPos.x) return "r";
  if (pos.y < prevPos.y) return "u";
  return "d";
}

// -------------------------------

function array2d(y, x, valueFunc) {
  return Array(y)
    .fill(0)
    .map(() =>
      Array(x)
        .fill(0)
        .map(() => valueFunc())
    );
}
