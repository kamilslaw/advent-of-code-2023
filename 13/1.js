const fs = require("fs");

console.log(
  fs
    .readFileSync("input.txt", "utf-8")
    .split(/\r?\n\r?\n/)
    .filter((str) => !!str)
    .map(parsePatterns)
    .map(getValue)
    .reduce((acc, x) => acc + x, 0)
);

function parsePatterns(str) {
  const lines = str.split(/\r?\n/);
  return {
    horizontal: [...lines],
    vertical: Array(lines[0].length)
      .fill(0)
      .map((_, i) =>
        Array(lines.length)
          .fill(0)
          .map((_, j) => lines[j][i])
          .join("")
      ),
  };
}

function getValue({ horizontal, vertical }) {
  return getReflectionPos(vertical) + getReflectionPos(horizontal) * 100;
}

function getReflectionPos(pattern) {
  for (let i = 0; i < pattern.length - 1; i += 1) {
    let allReflected = true;
    for (let diff = 0; diff < pattern.length; diff += 1) {
      const l = pattern[i - diff];
      const r = pattern[i + 1 + diff];
      if (l !== undefined && r !== undefined && l !== r) {
        allReflected = false;
        break;
      }
    }
    if (allReflected) return i + 1;
  }

  return 0;
}
