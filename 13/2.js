const fs = require("fs");

console.log(
  fs
    .readFileSync("input.txt", "utf-8")
    .split(/\r?\n\r?\n/)
    .filter((str) => !!str)
    .map(parsePatterns)
    .map(getValue)
    .reduce((acc, x) => acc + x, 0)
);

function parsePatterns(str) {
  const lines = str.split(/\r?\n/);
  return {
    horizontal: lines.map((l) => l.split("")),
    vertical: Array(lines[0].length)
      .fill(0)
      .map((_, i) =>
        Array(lines.length)
          .fill(0)
          .map((_, j) => lines[j][i])
      ),
  };
}

function getValue({ horizontal, vertical }) {
  return findReflectionPos(vertical) + findReflectionPos(horizontal) * 100;
}

function findReflectionPos(pattern) {
  const excluded = getReflectionPos(pattern);
  for (let i = 0; i < pattern.length; i += 1) {
    for (let j = 0; j < pattern[0].length; j += 1) {
      const originalValue = pattern[i][j];
      pattern[i][j] = originalValue === "#" ? "." : "#";
      const pos = getReflectionPos(pattern, excluded);
      pattern[i][j] = originalValue;
      if (pos) {
        if (i + 1 === pos) return pos;
        if (i + 1 < pos && pos - i - 1 < pattern.length - pos) return pos;
        if (i + 1 > pos && pattern.length - pos <= pos) return pos;
      }
    }
  }
  return 0;
}

function getReflectionPos(pattern, excluded) {
  for (let i = 0; i < pattern.length - 1; i += 1) {
    let allReflected = true;
    for (let diff = 0; diff < pattern.length; diff += 1) {
      const l = pattern[i - diff];
      const r = pattern[i + 1 + diff];
      if (l !== undefined && r !== undefined && !arraysEqual(l, r)) {
        allReflected = false;
        break;
      }
    }
    if (allReflected && i + 1 !== excluded) return i + 1;
  }

  return 0;
}

function arraysEqual(l, r) {
  return l.length === r.length && l.every((x, i) => x === r[i]);
}
