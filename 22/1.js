const fs = require("fs");

const cubes = normalizeCubes(
  fs
    .readFileSync("input.txt", "utf-8")
    .split(/\r?\n/)
    .filter((line) => !!line)
    .map(parseCube)
).sort((a, b) => a.from.z - b.from.z || a.to.z - b.to.z);

while (tryMove(cubes));

let result = 0;
for (const [index] of cubes.entries()) {
  const succeded = canBeSafelyDisintegrated(index, cubes);
  console.log(`checking ${index + 1}/${cubes.length}`, succeded);
  if (succeded) result += 1;
}
console.log("Result", result);

// -----------------------

function tryMove(cubes) {
  let hasSomethingBeenMoved = false;
  for (let i = 0; i < cubes.length; i += 1) {
    const cube = cubes[i];
    if (cube.from.z === 0) continue;
    const canBeMoved = !cubes.some(
      (c, n) =>
        n !== i && c.to.z === cube.from.z && overlapHorizontally(c, cube)
    );
    if (canBeMoved) {
      hasSomethingBeenMoved = true;
      cube.from.z -= 1;
      cube.to.z -= 1;
    }
  }
  return hasSomethingBeenMoved;
}

// -----------------------

function copyCube(cube) {
  return JSON.parse(JSON.stringify(cube));
}

// -----------------------

function overlapHorizontally(a, b) {
  if (
    a.to.x <= b.from.x ||
    b.to.x <= a.from.x ||
    a.to.y <= b.from.y ||
    b.to.y <= a.from.y
  )
    return false;
  return true;
}

// -----------------------

function canBeSafelyDisintegrated(i, cubes) {
  return !tryMove(cubes.filter((_, n) => i !== n).map(copyCube));
}

// -----------------------

function normalizeCubes(cubes) {
  const minX = Math.min(...cubes.map((c) => c.from.x));
  const minY = Math.min(...cubes.map((c) => c.from.y));
  if (minX < 0)
    cubes.forEach((c) => {
      c.from.x += Math.abs(minX);
      c.to.x += Math.abs(minX);
    });
  if (minY < 0)
    cubes.forEach((c) => {
      c.from.y += Math.abs(minY);
      c.to.y += Math.abs(minY);
    });
  return cubes;
}

// -----------------------

function parseCube(str) {
  const fromParts = str
    .split("~")[0]
    .split(",")
    .map((x) => parseInt(x));
  const toParts = str
    .split("~")[1]
    .split(",")
    .map((x) => parseInt(x));
  return {
    from: { x: fromParts[0] - 1, y: fromParts[1] - 1, z: fromParts[2] - 1 },
    to: { x: toParts[0], y: toParts[1], z: toParts[2] },
  };
}
