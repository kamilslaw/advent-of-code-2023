const fs = require("fs");

const platform = fs
  .readFileSync("input.txt", "utf-8")
  .split(/\r?\n/)
  .filter((line) => !!line)
  .map((line) => line.split(""));

const isRounded = (i, j) => platform[j][i] === "O";
const isAir = (i, j) => platform[j][i] === ".";
const moveUp = (i, j) => {
  platform[j][i] = ".";
  platform[j - 1][i] = "O";
};
const moveDown = (i, j) => {
  platform[j][i] = ".";
  platform[j + 1][i] = "O";
};
const moveLeft = (i, j) => {
  platform[j][i] = ".";
  platform[j][i - 1] = "O";
};
const moveRight = (i, j) => {
  platform[j][i] = ".";
  platform[j][i + 1] = "O";
};

for (let i = 1; i <= 1_000; i += 1) {
  console.log(i, calculateLoad());
  runCycle();
}

/* After print first 1000 cycles, we can see the pattern with repeating 9 values:

89 104520
90 104535
91 104528
92 104533
93 104537
94 104545
95 104544
96 104551
97 104519

so it will be (1_000_000_000 - 88 + 1) % 9 = 4, so the 4th element and the result is 104533

*/

function calculateLoad() {
  platform.reverse();
  let sum = 0;
  for (let i = 0; i < platform[0].length; i += 1)
    for (let j = 0; j < platform.length; j += 1)
      if (isRounded(i, j)) sum += j + 1;
  platform.reverse();
  return sum;
}

function runCycle() {
  // north
  for (let i = 0; i < platform[0].length; i += 1)
    for (let j = 0; j < platform.length; j += 1) {
      if (isRounded(i, j)) {
        let k = j;
        while (k > 0 && isAir(i, k - 1)) {
          moveUp(i, k);
          k -= 1;
        }
      }
    }
  // west
  for (let i = 0; i < platform[0].length; i += 1)
    for (let j = 0; j < platform.length; j += 1) {
      if (isRounded(i, j)) {
        let k = i;
        while (k > 0 && isAir(k - 1, j)) {
          moveLeft(k, j);
          k -= 1;
        }
      }
    }
  // south
  for (let i = 0; i < platform[0].length; i += 1)
    for (let j = platform.length - 1; j >= 0; j -= 1) {
      if (isRounded(i, j)) {
        let k = j;
        while (k < platform.length - 1 && isAir(i, k + 1)) {
          moveDown(i, k);
          k += 1;
        }
      }
    }
  // east
  for (let i = platform[0].length - 1; i >= 0; i -= 1)
    for (let j = 0; j < platform.length; j += 1) {
      if (isRounded(i, j)) {
        let k = i;
        while (k < platform[0].length - 1 && isAir(k + 1, j)) {
          moveRight(k, j);
          k += 1;
        }
      }
    }
}
