const fs = require("fs");

const platform = fs
  .readFileSync("input.txt", "utf-8")
  .split(/\r?\n/)
  .filter((line) => !!line)
  .map((line) => line.split(""));

const isRounded = (i, j) => platform[j][i] === "O";
const isAir = (i, j) => platform[j][i] === ".";
const moveUp = (i, j) => {
  platform[j][i] = ".";
  platform[j - 1][i] = "O";
};

for (let i = 0; i < platform[0].length; i += 1)
  for (let j = 0; j < platform.length; j += 1) {
    if (isRounded(i, j)) {
      let k = j;
      while (k > 0 && isAir(i, k - 1)) {
        moveUp(i, k);
        k -= 1;
      }
    }
  }

platform.reverse();

let sum = 0;
for (let i = 0; i < platform[0].length; i += 1)
  for (let j = 0; j < platform.length; j += 1)
    if (isRounded(i, j)) sum += j + 1;

console.log(sum);
