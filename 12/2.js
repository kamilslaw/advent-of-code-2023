const fs = require("fs");

console.log(
  fs
    .readFileSync("input.txt", "utf-8")
    .split(/\r?\n/)
    .filter((line) => !!line)
    .map((line) => {
      const s = line.split(" ")[0].split("");
      const r = line
        .split(" ")[1]
        .split(",")
        .map((x) => parseInt(x));
      return {
        springs: [...s, "?", ...s, "?", ...s, "?", ...s, "?", ...s],
        records: [...r, ...r, ...r, ...r, ...r],
      };
    })
    .map((row, i, rows) => {
      console.time(`${i + 1}/${rows.length}`);
      const solutions = getNumberOfPossibleSolutions(row);
      console.timeEnd(`${i + 1}/${rows.length}`);
      return solutions;
    })
    .reduce((acc, n) => acc + n, 0)
);

function getNumberOfPossibleSolutions({ springs, records }) {
  const positions = getPossiblePositions(springs, records);
  const tree = generateTree(positions, springs, records);
  return calculateFromTree(tree, positions.length - 2);
}

function calculateFromTree(tree, maxDepth) {
  const cache = [];
  for (let d = maxDepth; d >= -1; d -= 1) {
    cache[d] = [];
    tree[d].forEach(
      (range, i) =>
        (cache[d][i] =
          d == maxDepth
            ? range.length
            : range.map((r) => cache[d + 1][r]).reduce((acc, x) => acc + x, 0))
    );
  }
  return cache[-1][0];
}

function generateTree(positions, springs, records) {
  const indexesOk = (end, next) => {
    if (next - end < 2) return false;
    for (let s = end + 1; s < next; s += 1)
      if (springs[s] === "#") return false;
    return true;
  };

  const tree = { [-1]: [range(positions[0].length)] };
  for (let i = 0; i < positions.length - 1; i += 1) {
    tree[i] = [];
    for (const start of positions[i]) {
      const end = start + records[i] - 1;
      tree[i].push(
        positions[i + 1]
          .map((next, i) => (indexesOk(end, next) ? i : null))
          .filter((i) => i !== null)
      );
    }
  }
  return tree;
}

function getPossiblePositions(springs, records) {
  const lastHashLocation = springs.lastIndexOf("#");
  const realHashLocations = springs
    .map((s, i) => (s === "#" ? i : null))
    .filter((i) => i !== null)
    .reduce((acc, x) => {
      if (!acc.length) return [x];
      const record = records[acc.length - 1];
      const last = acc.at(-1);
      if (x - last >= record) return acc.concat([x]);
      return acc;
    }, []);
  const positions = [];
  for (let r = 0; r < records.length; r += 1) {
    const recordPositions = [];
    let startPos = 0;
    if (r > 0) startPos = positions[r - 1][0] + records[r - 1] + 1;
    if (r === records.length - 1 && startPos + records[r] <= lastHashLocation)
      startPos = lastHashLocation - records[r] + 1;
    let maxI = springs.length - records[r];
    for (let rr = r + 1; rr < records.length; rr += 1) maxI -= records[rr] + 1;
    if (r < realHashLocations.length)
      maxI = Math.min(maxI, realHashLocations[r]);
    for (let i = startPos; i <= maxI; i += 1) {
      let allGood = true;
      for (let j = i; j < i + records[r]; j += 1)
        if (springs[j] === ".") {
          allGood = false;
          break;
        }
      if (allGood) recordPositions.push(i);
    }
    positions.push(recordPositions);
  }
  return positions;
}

function range(n) {
  return Array(n)
    .fill(0)
    .map((_, i) => i);
}
