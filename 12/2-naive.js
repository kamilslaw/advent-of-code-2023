const fs = require("fs");

console.log(
  fs
    .readFileSync("input.txt", "utf-8")
    .split(/\r?\n/)
    .filter((line) => !!line)
    .map((line) => {
      const s = line.split(" ")[0].split("");
      const r = line
        .split(" ")[1]
        .split(",")
        .map((x) => parseInt(x));
      return {
        springs: [...s, "?", ...s, "?", ...s, "?", ...s, "?", ...s],
        records: [...r, ...r, ...r, ...r, ...r],
      };
    })
    .map((row) => {
      const lastHashLocation = row.springs.lastIndexOf("#");
      const realHashLocations = row.springs
        .map((s, i) => (s === "#" ? i : null))
        .filter((i) => i !== null)
        .reduce((acc, x) => {
          if (!acc.length) return [x];
          const record = row.records[acc.length - 1];
          const last = acc.at(-1);
          if (x - last >= record) return acc.concat([x]);
          return acc;
        }, []);
      const possiblePositions = [];
      for (let r = 0; r < row.records.length; r += 1) {
        const recordPositions = [];
        let startPos =
          r === 0 ? 0 : possiblePositions[r - 1][0] + row.records[r - 1] + 1;
        if (
          r === row.records.length - 1 &&
          startPos + row.records[r] < lastHashLocation
        )
          startPos = lastHashLocation - row.records[r];
        let maxI = row.springs.length - row.records[r];
        for (let rr = r + 1; rr < row.records.length; rr += 1)
          maxI -= row.records[rr] + 1;
        if (r < realHashLocations.length)
          maxI = Math.min(maxI, realHashLocations[r]);
        for (let i = startPos; i <= maxI; i += 1) {
          if (i > 0 && row.springs[i - 1] === "#") continue;
          let allGood = true;
          for (let j = i; j < i + row.records[r]; j += 1)
            if (row.springs[j] === ".") {
              allGood = false;
              break;
            }
          if (allGood) recordPositions.push(i);
        }
        possiblePositions.push(recordPositions);
      }
      return { ...row, possiblePositions };
    })
    .map((row, i, rows) => {
      console.time(`${i + 1}/${rows.length}`);
      const { solutions, iterations } = getNumberOfPossibleSolutions(row);
      console.timeEnd(`${i + 1}/${rows.length}`);
      console.log(`     iterations`, iterations, "solutions", solutions);
      return solutions;
    })
    .reduce((acc, n) => acc + n, 0)
);

function getNumberOfPossibleSolutions({ springs, records, possiblePositions }) {
  const getNextPos = (index, startPos) => {
    const result = possiblePositions[index].find((p) => p >= startPos);
    return result === undefined ? null : result;
  };

  let solutions = 0;
  let iterations = 0;

  const indexes = [getNextPos(0, 0)];
  for (let r = 1; r < records.length; r += 1)
    indexes.push(getNextPos(r, indexes[r - 1] + records[r - 1] + 1));

  while (true) {
    iterations += 1;
    const validationResult = isValid(springs, indexes, records);
    if (validationResult === true) solutions += 1;

    let indexToMove = null;
    let nextIndexes = [];
    const indexToStart =
      validationResult === true ? indexes.length - 1 : validationResult;
    for (let i = indexToStart; i >= 0; i -= 1) {
      nextIndexes = [getNextPos(i, indexes[i] + 1)];
      if (nextIndexes[0] === null) continue;
      for (let r = i + 1; r < records.length; r += 1) {
        nextIndexes.push(
          getNextPos(r, nextIndexes.at(-1) + records[r - 1] + 1)
        );
        if (nextIndexes.at(-1) === null) break;
      }
      if (nextIndexes.at(-1) === null) continue;
      indexToMove = i;
      break;
    }
    if (indexToMove === null) break;
    for (let i = indexToMove; i < indexes.length; i += 1)
      indexes[i] = nextIndexes[i - indexToMove];
  }

  return { solutions, iterations };
}

function isValid(springs, indexes, records) {
  let currentIndex = 0;
  let currentLeft = records[currentIndex];
  let noneLeft = false;
  for (let s = 0; s < springs.length; s += 1) {
    if (currentLeft === 0) {
      currentIndex += 1;
      currentLeft = records[currentIndex];
      if (currentLeft === undefined) noneLeft = true;
    }
    if (noneLeft || s < indexes[currentIndex]) {
      if (springs[s] === "#") return currentIndex - 1;
    } else {
      if (springs[s] === ".") return currentIndex - 1;
      currentLeft -= 1;
    }
  }

  return true;
}
