const fs = require("fs");

console.log(
  fs
    .readFileSync("input.txt", "utf-8")
    .split(/\r?\n/)
    .filter((line) => !!line)
    .map((line) => {
      const parts = line.split(" ");
      const records = parts[1].split(",").map((x) => parseInt(x));
      const recordsSum = records.reduce((acc, r) => acc + r, 0);
      return {
        springs: parts[0].split(""),
        records,
        recordsSum,
      };
    })
    .map((row, i, rows) => {
      console.time(`${i + 1}/${rows.length}`);
      const result = getNumberOfPossibleSolutions(row);
      console.timeEnd(`${i + 1}/${rows.length}`);
      return result;
    })
    .reduce((acc, n) => acc + n, 0)
);

function getNumberOfPossibleSolutions({ springs, records, recordsSum }) {
  let possibleSolutions = 0;

  const indexes = [0];
  for (let i = 1; i < records.length; i += 1)
    indexes.push(indexes[i - 1] + records[i - 1] + 1);

  while (true) {
    if (isValid(springs, indexes, records, recordsSum)) possibleSolutions += 1;

    let indexToMove = null;
    for (let i = indexes.length - 1; i >= 0; i -= 1) {
      const pos = indexes[i];
      const availableSpace = springs.length - pos - 1;
      let requiredSpace = -1;
      for (let r = i; r < records.length; r += 1)
        requiredSpace += records[r] + 1;
      if (requiredSpace <= availableSpace) {
        indexToMove = i;
        break;
      }
    }
    if (indexToMove === null) break;
    indexes[indexToMove] += 1;
    for (let i = indexToMove + 1; i < indexes.length; i += 1)
      indexes[i] = indexes[i - 1] + records[i - 1] + 1;
  }

  return possibleSolutions;
}

function isValid(springs, indexes, records, recordsSum) {
  const decoded = springs.map((s, sI) => {
    if (s !== "?") return s;
    const hasRelatedRecord = records.some(
      (r, rI) => sI >= indexes[rI] && sI < indexes[rI] + r
    );
    return hasRelatedRecord ? "#" : ".";
  });

  for (let i = 0; i < indexes.length; i += 1)
    for (let j = indexes[i]; j < indexes[i] + records[i]; j += 1)
      if (decoded[j] !== "#") return false;

  return decoded.filter((s) => s === "#").length === recordsSum;
}
