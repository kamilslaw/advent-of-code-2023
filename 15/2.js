const fs = require("fs");

console.log(
  fs
    .readFileSync("input.txt", "utf-8")
    .split(/\r?\n/)[0]
    .split(",")
    .reduce(
      (boxes, x) =>
        x.endsWith("-")
          ? removeFromBoxes(boxes, x.replace("-", ""))
          : addToBoxes(boxes, x.split("=")[0], parseInt(x.split("=")[1])),
      Array(256)
        .fill(0)
        .map(() => [])
    )
    .reduce((acc, box, i) => acc + getFocusingPower(box, i), 0)
);

function removeFromBoxes(boxes, lensName) {
  const boxIndex = getHash(lensName);
  boxes[boxIndex] = boxes[boxIndex].filter((l) => l.name !== lensName);
  return boxes;
}

function addToBoxes(boxes, lensName, lensFocalLength) {
  const boxIndex = getHash(lensName);
  const lensIndex = boxes[boxIndex].findIndex((l) => l.name === lensName);
  if (lensIndex >= 0) boxes[boxIndex][lensIndex].focalLength = lensFocalLength;
  else boxes[boxIndex].push({ name: lensName, focalLength: lensFocalLength });
  return boxes;
}

function getFocusingPower(box, boxNumber) {
  return box.reduce(
    (acc, lens, i) => acc + (boxNumber + 1) * (i + 1) * lens.focalLength,
    0
  );
}

function getHash(str) {
  let hash = 0;
  for (let i = 0; i < str.length; i += 1) {
    hash += str.charCodeAt(i);
    hash *= 17;
    hash %= 256;
  }
  return hash;
}
