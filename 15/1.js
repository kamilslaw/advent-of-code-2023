const fs = require("fs");

console.log(
  fs
    .readFileSync("input.txt", "utf-8")
    .split(/\r?\n/)[0]
    .split(",")
    .map(getHash)
    .reduce((acc, x) => acc + x, 0)
);

function getHash(str) {
  let hash = 0;
  for (let i = 0; i < str.length; i += 1) {
    hash += str.charCodeAt(i);
    hash *= 17;
    hash %= 256;
  }
  return hash;
}
