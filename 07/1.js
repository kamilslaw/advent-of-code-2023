const fs = require("fs");

const CARDS = ["A", "K", "Q", "J", "T", "9", "8", "7", "6", "5", "4", "3", "2"];
const TYPES = [
  fiveOfKind,
  fourOfKind,
  fullHouse,
  threeOfKind,
  twoPair,
  onePair,
  highCard,
];

console.log(
  fs
    .readFileSync("input.txt", "utf-8")
    .split(/\r?\n/)
    .filter((line) => !!line)
    .map((l) => {
      const parts = l.split(" ");
      return { hand: parts[0].split(""), bid: parseInt(parts[1]) };
    })
    .sort((a, b) => compareHands(b.hand, a.hand))
    .reduce((acc, x, index) => acc + x.bid * (index + 1), 0)
);

function compareHands(a, b) {
  return compareHandsType(a, b) || compareHandsOrder(a, b);
}

function compareHandsType(a, b) {
  return TYPES.findIndex((t) => t(a)) - TYPES.findIndex((t) => t(b));
}

function compareHandsOrder(a, b) {
  for (let i = 0; i < a.length; i += 1)
    if (a[i] !== b[i]) return CARDS.indexOf(a[i]) - CARDS.indexOf(b[i]);
  return 0;
}

function fiveOfKind(hand) {
  return arraysEqual(countUnique(hand), [5]);
}

function fourOfKind(hand) {
  return arraysEqual(countUnique(hand), [4, 1]);
}

function fullHouse(hand) {
  return arraysEqual(countUnique(hand), [3, 2]);
}

function threeOfKind(hand) {
  return arraysEqual(countUnique(hand), [3, 1, 1]);
}

function twoPair(hand) {
  return arraysEqual(countUnique(hand), [2, 2, 1]);
}

function onePair(hand) {
  return arraysEqual(countUnique(hand), [2, 1, 1, 1]);
}

function highCard(hand) {
  return arraysEqual(countUnique(hand), [1, 1, 1, 1, 1]);
}

function countUnique(array) {
  const counts = {};
  for (let i = 0; i < array.length; i += 1)
    counts[array[i]] = 1 + (counts[array[i]] || 0);
  return Object.values(counts).sort((a, b) => b - a);
}

function arraysEqual(a, b) {
  if (a.length !== b.length) return false;
  return a.every((x, i) => x === b[i]);
}
