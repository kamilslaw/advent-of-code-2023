const fs = require("fs");

const CARDS = ["A", "K", "Q", "T", "9", "8", "7", "6", "5", "4", "3", "2", "J"];
const TYPES = [
  fiveOfKind,
  fourOfKind,
  fullHouse,
  threeOfKind,
  twoPair,
  onePair,
  highCard,
];

console.log(
  fs
    .readFileSync("input.txt", "utf-8")
    .split(/\r?\n/)
    .filter((line) => !!line)
    .map((l) => {
      const parts = l.split(" ");
      return {
        hand: parts[0].split(""),
        improvedHand: improveHand(parts[0].split("")),
        bid: parseInt(parts[1]),
      };
    })
    .sort((a, b) => compareHands(b, a))
    .reduce((acc, x, index) => acc + x.bid * (index + 1), 0)
);

function compareHands(a, b) {
  return (
    compareHandsType(a.improvedHand, b.improvedHand) ||
    compareHandsOrder(a.hand, b.hand)
  );
}

function compareHandsType(a, b) {
  const aa = improveHand(a);
  const bb = improveHand(b);
  return TYPES.findIndex((t) => t(aa)) - TYPES.findIndex((t) => t(bb));
}

function compareHandsOrder(a, b) {
  for (let i = 0; i < a.length; i += 1)
    if (a[i] !== b[i]) return CARDS.indexOf(a[i]) - CARDS.indexOf(b[i]);
  return 0;
}

function fiveOfKind(hand) {
  return arraysEqual(countUnique(hand), [5]);
}

function fourOfKind(hand) {
  return arraysEqual(countUnique(hand), [4, 1]);
}

function fullHouse(hand) {
  return arraysEqual(countUnique(hand), [3, 2]);
}

function threeOfKind(hand) {
  return arraysEqual(countUnique(hand), [3, 1, 1]);
}

function twoPair(hand) {
  return arraysEqual(countUnique(hand), [2, 2, 1]);
}

function onePair(hand) {
  return arraysEqual(countUnique(hand), [2, 1, 1, 1]);
}

function highCard(hand) {
  return arraysEqual(countUnique(hand), [1, 1, 1, 1, 1]);
}

function countUnique(array) {
  const counts = {};
  for (let i = 0; i < array.length; i += 1)
    counts[array[i]] = 1 + (counts[array[i]] || 0);
  return Object.values(counts).sort((a, b) => b - a);
}

function arraysEqual(a, b) {
  if (a.length !== b.length) return false;
  return a.every((x, i) => x === b[i]);
}

function improveHand(hand) {
  if (!hand.includes("J")) return hand;

  if (hand.every((c) => c === "J")) return Array(hand.length).fill(CARDS[0]);

  const counts = {};
  for (let i = 0; i < hand.length; i += 1)
    if (hand[i] !== "J") counts[hand[i]] = 1 + (counts[hand[i]] || 0);
  const bestCard = Object.keys(counts).sort((a, b) => counts[b] - counts[a])[0];
  return hand.map((c) => (c !== "J" ? c : bestCard));
}
