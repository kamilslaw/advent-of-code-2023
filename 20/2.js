const fs = require("fs");
const { exit } = require("process");

const lines = fs
  .readFileSync("input.txt", "utf-8")
  .split(/\r?\n/)
  .filter((line) => !!line);

const modules = {};

for (const [index, line] of lines.entries()) {
  const outputs = line.split("-> ")[1].split(", ");
  const name = getName(line);
  modules[name] = line.startsWith("%")
    ? flipFlop(name, outputs)
    : line.startsWith("&")
    ? conjunction(
        name,
        outputs,
        lines.filter((l, i) => i !== index && l.includes(name)).map(getName)
      )
    : broadcaster(name, outputs);
}

for (let i = 0; i < 10_000; i += 1) {
  const outputs = modules.broadcaster.receive(0);
  while (outputs.length) {
    const outputsCopy = [...outputs];
    outputs.length = 0;
    outputsCopy.forEach(([target, pulse, source]) => {
      if (["sx", "jt", "kb", "ks"].includes(source) && pulse) {
        console.log(source + " triggered", i + 1);
      }
      if (!modules[target]) return;
      modules[target].receive(pulse, source).forEach((o) => outputs.push(o));
    });
  }
}

// THE CODE ABOVE PRINTED
// kb triggered 3851
// sx triggered 3877
// ks triggered 4021
// jt triggered 4049
// ALL THE MODULES ABOVE (&kb, &sx, &ks, &jt) HAVE TO BE TRUE TO TRIGGER THE &zh,
// WHICH WILL TRIGGER THE rx AND END THE PROGRAM
// THE ANSWER IS THE LEAST COMMON MULTIPLE OF THE NUMBERS ABOVE, WHICH IS
// 243 081 086 866 483



function getName(line) {
  return line.split(" ->")[0].replace("%", "").replace("&", "");
}

function broadcaster(name, outputs) {
  return {
    receive: (pulse) => outputs.map((o) => [o, pulse, name]),
  };
}

function flipFlop(name, outputs) {
  return {
    state: false,
    receive: function (pulse) {
      if (!pulse) {
        this.state = !this.state;
        pulse = this.state;
        return outputs.map((o) => [o, pulse, name]);
      }
      return [];
    },
  };
}

function conjunction(name, outputs, inputs) {
  return {
    inputs: inputs.reduce((acc, i) => {
      acc[i] = false;
      return acc;
    }, {}),
    receive: function (pulse, input) {
      this.inputs[input] = pulse;
      pulse = Object.values(this.inputs).every((p) => p) ? 0 : 1;
      return outputs.map((o) => [o, pulse, name]);
    },
  };
}
