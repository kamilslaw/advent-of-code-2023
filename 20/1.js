const fs = require("fs");

const lines = fs
  .readFileSync("input.txt", "utf-8")
  .split(/\r?\n/)
  .filter((line) => !!line);

const modules = {};

for (const [index, line] of lines.entries()) {
  const outputs = line.split("-> ")[1].split(", ");
  const name = getName(line);
  modules[name] = line.startsWith("%")
    ? flipFlop(name, outputs)
    : line.startsWith("&")
    ? conjunction(
        name,
        outputs,
        lines.filter((l, i) => i !== index && l.includes(name)).map(getName)
      )
    : broadcaster(name, outputs);
}

let lowPulses = 0;
let highPulses = 0;
for (let i = 0; i < 1000; i += 1) {
  lowPulses += 1;
  const outputs = modules.broadcaster.receive(0);
  while (outputs.length) {
    const outputsCopy = [...outputs];
    outputs.length = 0;
    outputsCopy.forEach(([target, pulse, source]) => {
      if (pulse) highPulses += 1;
      else lowPulses += 1;
      if (!modules[target]) return;
      // console.log(`${source} -${pulse ? "high" : "low"}-> ${target}`);
      modules[target].receive(pulse, source).forEach((o) => outputs.push(o));
    });
  }
}
console.log(lowPulses, highPulses, lowPulses * highPulses);

function getName(line) {
  return line.split(" ->")[0].replace("%", "").replace("&", "");
}

function broadcaster(name, outputs) {
  return {
    receive: (pulse) => outputs.map((o) => [o, pulse, name]),
  };
}

function flipFlop(name, outputs) {
  return {
    state: false,
    receive: function (pulse) {
      if (!pulse) {
        this.state = !this.state;
        pulse = this.state;
        return outputs.map((o) => [o, pulse, name]);
      }
      return [];
    },
  };
}

function conjunction(name, outputs, inputs) {
  return {
    inputs: inputs.reduce((acc, i) => {
      acc[i] = false;
      return acc;
    }, {}),
    receive: function (pulse, input) {
      this.inputs[input] = pulse;
      pulse = Object.values(this.inputs).every((p) => p) ? 0 : 1;
      return outputs.map((o) => [o, pulse, name]);
    },
  };
}
