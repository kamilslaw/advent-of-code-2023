const fs = require("fs");

const EXPANSION_RATE = 1_000_000;

const lines = fs
  .readFileSync("input.txt", "utf-8")
  .split(/\r?\n/)
  .filter((line) => !!line)
  .map((line) => line.split(""));

let space = {
  fields: lines,
  expansions: {
    horizontal: Array(lines.length).fill(1),
    vertical: Array(lines[0].length).fill(1),
  },
};

space = expandSpace(space, EXPANSION_RATE);

console.log(
  locateGalaxies(space)
    .flatMap((galaxy, _, galaxies) => getDistances(galaxy, galaxies, space))
    .reduce((acc, x) => acc + x, 0) / 2
);

// --------------------------

function isEmpty(field) {
  return field === ".";
}

function isGalaxy(field) {
  return field === "#";
}

// --------------------------

function expandSpace(space, expansionRate) {
  return expandSpaceHorizontally(
    expandSpaceVertically(space, expansionRate),
    expansionRate
  );
}

function expandSpaceVertically(space, expansionRate) {
  space.fields.forEach((row, i) => {
    if (row.every(isEmpty)) space.expansions.horizontal[i] = expansionRate;
  });
  return space;
}

function expandSpaceHorizontally(space, expansionRate) {
  for (let x = 0; x < space.fields[0].length; x += 1)
    if (space.fields.every((row) => isEmpty(row[x])))
      space.expansions.vertical[x] = expansionRate;
  return space;
}

// --------------------------

function locateGalaxies({ fields }) {
  const galaxies = [];
  for (let y = 0; y < fields.length; y += 1)
    for (let x = 0; x < fields[0].length; x += 1)
      if (isGalaxy(fields[y][x])) galaxies.push({ y, x });
  return galaxies;
}

// --------------------------

function getDistances(galaxy, galaxies, { expansions }) {
  return galaxies.map((g) => {
    const startX = Math.min(galaxy.x, g.x);
    const endX = Math.max(galaxy.x, g.x);
    const startY = Math.min(galaxy.y, g.y);
    const endY = Math.max(galaxy.y, g.y);
    let distance = 0;
    for (let x = startX + 1; x <= endX; x += 1)
      distance += expansions.vertical[x];
    for (let y = startY + 1; y <= endY; y += 1)
      distance += expansions.horizontal[y];
    return distance;
  });
}
