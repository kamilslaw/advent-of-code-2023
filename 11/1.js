const fs = require("fs");

const space = fs
  .readFileSync("input.txt", "utf-8")
  .split(/\r?\n/)
  .filter((line) => !!line)
  .map((line) => line.split(""));

console.log(
  locateGalaxies(expandSpace(space))
    .flatMap(calculateDistancesToOtherGalaxies)
    .reduce((acc, x) => acc + x, 0) / 2
);

// --------------------------

function isEmpty(field) {
  return field === ".";
}

function isGalaxy(field) {
  return field === "#";
}

// --------------------------

function expandSpace(space) {
  return expandSpaceHorizontally(expandSpaceVertically(space));
}

function expandSpaceVertically(space) {
  return space.flatMap((row) => (row.every(isEmpty) ? [row, [...row]] : [row]));
}

function expandSpaceHorizontally(space) {
  for (let x = 0; x < space[0].length; x += 1)
    if (space.every((row) => isEmpty(row[x]))) {
      space.forEach((row) => row.splice(x, 0, "."));
      x += 1;
    }
  return space;
}

// --------------------------

function locateGalaxies(space) {
  const galaxies = [];
  for (let y = 0; y < space.length; y += 1)
    for (let x = 0; x < space[0].length; x += 1)
      if (isGalaxy(space[y][x])) galaxies.push({ y, x });
  return galaxies;
}

// --------------------------

function calculateDistancesToOtherGalaxies(galaxy, index, galaxies) {
  return galaxies
    .filter((_, i) => i !== index)
    .map((g) => Math.abs(galaxy.x - g.x) + Math.abs(galaxy.y - g.y));
}
