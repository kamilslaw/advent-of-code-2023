const fs = require("fs");

Array.prototype.groupEveryN = function (n) {
  return this.reduce(
    (acc, x) => {
      const last = acc.at(-1);
      if (last.length === n) return acc.concat([[x]]);
      last.push(x);
      return acc;
    },
    [[]]
  );
};

const lines = fs.readFileSync("input.txt", "utf-8").split(/\r?\n/);

const seeds = lines[0]
  .split(": ")[1]
  .split(" ")
  .map((s) => parseInt(s))
  .groupEveryN(2)
  .map((gr) => ({ min: gr[0], max: gr[0] + gr[1] }));

const maps = lines
  .slice(2)
  .join("\r\n")
  .split(/\r\n\r\n/)
  .map((s) => s.split(/\r\n/))
  .map((s) => s.slice(1))
  .map((s) =>
    s.map((ss) => {
      const arr = ss.split(" ");
      const destination = parseInt(arr[0]);
      const source = parseInt(arr[1]);
      const length = parseInt(arr[2]);
      return {
        diff: destination - source,
        destMin: destination,
        destMax: destination + length,
      };
    })
  )
  .reverse();

let minLocation = 0;
while (!isValid(minLocation)) {
  minLocation += 1;
  if (minLocation % 10_000_000 === 0) console.log(minLocation + "...");
}

console.log(`# Solution: ${minLocation}`);

function isValid(location) {
  const startValue = maps.reduce(convert, location);
  return seeds.some((s) => s.min <= startValue && startValue <= s.max);
}

function convert(location, map) {
  const mapEntry = map.find((e) => isInside(location, e));
  return mapEntry ? location - mapEntry.diff : location;
}

function isInside(location, mapEntry) {
  return mapEntry.destMin <= location && location <= mapEntry.destMax;
}
