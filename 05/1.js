const fs = require("fs");

const lines = fs.readFileSync("input.txt", "utf-8").split(/\r?\n/);

const seeds = lines[0]
  .split(": ")[1]
  .split(" ")
  .map((s) => parseInt(s));

const maps = lines
  .slice(2)
  .join("\r\n")
  .split(/\r\n\r\n/)
  .map((s) => s.split(/\r\n/))
  .map((s) => s.slice(1))
  .map((s) =>
    s.map((ss) => {
      const arr = ss.split(" ");
      const destination = parseInt(arr[0]);
      const source = parseInt(arr[1]);
      const length = parseInt(arr[2]);
      return {
        diff: source - destination,
        sourceMin: source,
        sourceMax: source + length,
      };
    })
  );

console.log(Math.min(...seeds.map((s) => maps.reduce(convert, s))));

function convert(seed, map) {
  const mapEntry = map.find((e) => isInside(seed, e));
  return mapEntry ? seed - mapEntry.diff : seed;
}

function isInside(seed, mapEntry) {
  return mapEntry.sourceMin <= seed && seed <= mapEntry.sourceMax;
}
