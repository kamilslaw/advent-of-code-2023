const fs = require("fs");

Array.prototype.groupEveryN = function (n) {
  return this.reduce(
    (acc, x) => {
      const last = acc.at(-1);
      if (last.length === n) return acc.concat([[x]]);
      last.push(x);
      return acc;
    },
    [[]]
  );
};

const lines = fs.readFileSync("test-input.txt", "utf-8").split(/\r?\n/);

const seeds = lines[0]
  .split(": ")[1]
  .split(" ")
  .map((s) => parseInt(s))
  .groupEveryN(2)
  .flatMap((gr) => {
    const start = gr[0];
    const length = gr[1];
    return Array(length)
      .fill(0)
      .map((_, index) => start + index);
  });

const maps = lines
  .slice(2)
  .join("\r\n")
  .split(/\r\n\r\n/)
  .map((s) => s.split(/\r\n/))
  .map((s) => s.slice(1))
  .map((s) =>
    s.map((ss) => {
      const arr = ss.split(" ");
      const destination = parseInt(arr[0]);
      const source = parseInt(arr[1]);
      const length = parseInt(arr[2]);
      return {
        diff: source - destination,
        sourceMin: source,
        sourceMax: source + length,
      };
    })
  );

console.log(Math.min(...seeds.map((s) => maps.reduce(convert, s))));

function convert(seed, map) {
  const mapEntry = map.find((e) => isInside(seed, e));
  return mapEntry ? seed - mapEntry.diff : seed;
}

function isInside(seed, mapEntry) {
  return mapEntry.sourceMin <= seed && seed <= mapEntry.sourceMax;
}
