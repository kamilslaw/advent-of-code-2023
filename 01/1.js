const fs = require("fs");

const ZERO_CHAR_CODE = 48;
const NINE_CHAR_CODE = 57;

console.log(
  fs
    .readFileSync("input.txt", "utf-8")
    .split(/\r?\n/)
    .filter((line) => !!line)
    .map((line) => parseInt(getFirstDigitAsStr(line) + getLastDigitAsStr(line)))
    .reduce((acc, x) => acc + x, 0)
);

function getFirstDigitAsStr(str) {
  for (const c of str.split("")) if (isDigit(c)) return c;
}

function getLastDigitAsStr(str) {
  return getFirstDigitAsStr(reverseStr(str));
}

function isDigit(char) {
  const code = char.charCodeAt(0);
  return ZERO_CHAR_CODE <= code && code <= NINE_CHAR_CODE;
}

function reverseStr(str) {
  return str.split("").reverse().join("");
}
