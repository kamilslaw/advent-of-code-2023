const fs = require("fs");

const ZERO_CHAR_CODE = 48;
const NINE_CHAR_CODE = 57;
const DIGITS_SPELLED = [
  "zero",
  "one",
  "two",
  "three",
  "four",
  "five",
  "six",
  "seven",
  "eight",
  "nine",
];

console.log(
  fs
    .readFileSync("input.txt", "utf-8")
    .split(/\r?\n/)
    .filter((line) => !!line)
    .map((line) => translateSpelledDigitsIntoChars(line))
    .map((line) => parseInt(getFirstDigitAsStr(line) + getLastDigitAsStr(line)))
    .reduce((acc, x) => acc + x, 0)
);

function getFirstDigitAsStr(str) {
  for (const c of str.split("")) if (isDigit(c)) return c;
}

function getLastDigitAsStr(str) {
  return getFirstDigitAsStr(reverseStr(str));
}

function isDigit(char) {
  const code = char.charCodeAt(0);
  return ZERO_CHAR_CODE <= code && code <= NINE_CHAR_CODE;
}

function reverseStr(str) {
  return str.split("").reverse().join("");
}

function translateSpelledDigitsIntoChars(str) {
  let tmpStr = "";
  for (let i = 0; i < str.length; i += 1) {
    if (isDigit(str[i])) {
      tmpStr = "";
    } else {
      tmpStr += str[i];
      const digitIndex = DIGITS_SPELLED.findIndex((d) => tmpStr.includes(d));
      if (digitIndex >= 0) {
        str = str.replace(tmpStr, digitIndex);
        i = 0;
        tmpStr = "";
      }
    }
  }
  return str;
}
